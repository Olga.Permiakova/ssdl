
# Test environments

* `Ubuntu 18.04.5`: release
* Rhub `Debian`: release, devel
* Rhub `Windows`: release, devel

# R CMD check --as-cran
0 ERRORS | 0 WARNINGS | 1 NOTES

* NOTE: checking CRAN incoming feasibility 
          Maintainer: ‘Olga Permiakova <olga.permiakova@gmail.com>’
          New submission

# SSDL re-submission on the CRAN website
CRAN check detected two NOTES:
 
* checking for detritus in the temp directory ... NOTE
  Found the following files/directories:
  ‘RtmpIJ1I52Data_tvmax.bk’ ‘RtmpIJ1I52Data_tvmax.rds’
  ‘RtmpIJ1I52Data_tvmaxnotcorr.bk’ ‘RtmpIJ1I52Data_tvmaxnotcorr.rds’
  
> These are auxiliary files from the TV_initialization function. I have added file.remove () to the end of this function to remove them.

* checking CRAN incoming feasibility ... NOTE
Maintainer: ‘Olga Permiakova <olga.permiakova@gmail.com>’

New submission

Possibly mis-spelled words in DESCRIPTION:
  Permiakova (10:65)
  spectrometry (12:7)

Found the following (possibly) invalid URLs:
  URL: https://https://gitlab.com/Olga.Permiakova/ssdl/
    From: README.md
    Status: Error
    Message: Could not resolve host: https
    
> The words 'spectrometry' and Permiakova are spelled correctly. The URL in the README.md file has been corrected to https://gitlab.com/Olga.Permiakova/ssdl/. 